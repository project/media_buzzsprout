<?php

/**
 * @file
 * Template file for theme('media_buzzsprout_audio').
 *
 * Variables available:
 *  $account_id = The Buzzsprout account ID for this podcast.
 *  $episode_id = The Buzzsprout ID for this episode.
 *  $container_id = The html id for the Buzzsprout player container.
 *  $uri - The media uri for the Buzzsprout audio
 *    (e.g., buzzsprout://a/[uaccount_id]/e/[episode_id]).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query string for the Buzzsprout script source.
 *  $options - An array containing the Media Buzzsprout formatter options.
 *  $file - The Media Buzzsprout file object.
 */
?>
<div id="<?php print $container_id; ?>" class="<?php print $classes; ?>"></div>
<script src="<?php print $script_url; ?>" type="text/javascript" charset="utf-8"></script>
