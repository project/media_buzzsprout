<?php

/**
 * @file
 * Theme and preprocess functions for Media: Buzzsprout.
 */

/**
 * Preprocess function for theme('media_buzzsprout_audio').
 */
function media_buzzsprout_preprocess_media_buzzsprout_audio(&$variables) {
  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();

  $variables['account_id'] = isset($parts['a']) ? check_plain($parts['a']) : '';
  $variables['episode_id'] = isset($parts['e']) ? check_plain($parts['e']) : '';
  $variables['container_id'] = 'buzzsprout-player-' . $variables['episode_id'];

  $path = '/a/' . $variables['account_id'] . '/e/' . $variables['episode_id'];
  $uri = file_stream_wrapper_uri_normalize('buzzsprout://' . $path);

  // Make the file object available.
  $variables['file'] = file_uri_to_object($uri);

  $variables['url'] = file_create_url($uri);
  $query = [
    'container_id' => $variables['container_id'],
    'player' => 'small',
  ];
  // Build the script URL with options query string.
  $variables['script_url'] = url($variables['url'] . '.js', array('query' => $query, 'external' => TRUE));

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }
}
