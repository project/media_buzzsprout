<?php

/**
 * @file
 * Default display configuration for the default file types.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_buzzsprout_file_default_displays() {
  $file_displays = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__default__media_buzzsprout_audio';
  $file_display->weight = 1;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $file_displays['audio__default__media_buzzsprout_audio'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__preview__media_buzzsprout_image';
  $file_display->weight = 2;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'media_thumbnail',
  );
  $file_displays['audio__preview__media_buzzsprout_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__teaser__media_buzzsprout_audio';
  $file_display->weight = 1;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $file_displays['audio__teaser__media_buzzsprout_audio'] = $file_display;

  return $file_displays;
}
