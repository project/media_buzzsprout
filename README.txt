CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage

INTRODUCTION
------------

Current Maintainers:

 * Jeremy Stoller <http://drupal.org/user/99012>

Media: Buzzsprout adds Buzzsprout as a supported media provider.

REQUIREMENTS
------------

Media: Buzzsprout has one dependency.

Contributed modules
 * Media Internet - A submodule of the Media module.

INSTALLATION
------------

Media: Buzzsprout can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).

USAGE
-----

Media: Buzzsprout integrates the Buzzsprout podcast hosting service with the
Media module to allow users to add and manage Buzzsprout audio as they would
any other piece of media.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With Media: Buzzsprout enabled, users can add Buzzsprout audio by entering its
URL.
