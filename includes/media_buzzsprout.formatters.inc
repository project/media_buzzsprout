<?php

/**
 * @file
 * File formatters for Buzzsprout audio.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_buzzsprout_file_formatter_info() {
  $formatters['media_buzzsprout_audio'] = [
    'label' => t('Buzzsprout Audio'),
    'file types' => ['audio'],
    'default settings' => [],
    'view callback' => 'media_buzzsprout_file_formatter_audio_view',
    'settings callback' => 'media_buzzsprout_file_formatter_audio_settings',
    'mime types' => ['audio/buzzsprout'],
  ];

  $formatters['media_buzzsprout_image'] = [
    'label' => t('Buzzsprout Preview Image'),
    'file types' => ['audio'],
    'default settings' => [
      'image_style' => '',
    ],
    'view callback' => 'media_buzzsprout_file_formatter_image_view',
    'settings callback' => 'media_buzzsprout_file_formatter_image_settings',
    'mime types' => ['audio/buzzsprout'],
  ];

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_buzzsprout_file_formatter_audio_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'buzzsprout') {
    $element = [
      '#theme' => 'media_buzzsprout_audio',
      '#uri' => $file->uri,
      '#options' => [],
    ];

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = [];

    foreach (['attributes'] as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_buzzsprout_file_formatter_audio_settings($form, &$form_state, $settings) {
  $element = [];

  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_buzzsprout_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'buzzsprout') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);

    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = [
        '#theme' => 'image',
        '#path' => $wrapper->getLocalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      ];
    }
    else {
      $element = [
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      ];
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_buzzsprout_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = [];

  $element['image_style'] = [
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  ];

  return $element;
}
