<?php

/**
 * @file
 * Implements a stream wrapper class for Buzzsprout audio.
 */

/**
 * Extends the MediaReadOnlyStreamWrapper class to handle Buzzsprout audio.
 *
 * Create an instance like this:
 * $buzzsprout = new MediaBuzzsproutStreamWrapper('buzzsprout://a/[podcast-id]/e/[episode-id]');
 */
class MediaBuzzsproutStreamWrapper extends MediaReadOnlyStreamWrapper {
  /**
   * Base URL for Buzzsprout requests.
   *
   * @var string
   */
  protected $base_url = 'https://www.buzzsprout.com/';

  /**
   * Parameters in buzzsprout stream wrapper URL.
   *
   * @var array
   */
  protected $parameters = ['a', 'e'];

  /**
   * Buzzsprout implementation of getMimeType().
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'audio/buzzsprout';
  }

  /**
   * Handles parameters on the URL string.
   */
  public function interpolateUrl() {
    return $this->base_url . check_plain($this->parameters['a']) . '/' . check_plain($this->parameters['e']);
  }

  /**
   * Get the URL to a thumbnail image for this file.
   */
  public function getOriginalThumbnailPath() {
    // @todo Figure out how to get the actual podcast cover art instead of using
    // the default Buzzsprout image.
    return 'https://www.buzzsprout.com/images/artworks_cover.jpg';
  }

  /**
   * Get the path to a local thumbnail image for this file.
   */
  public function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = file_default_scheme() . '://media-buzzsprout/';

    if (isset($parts['a']) && isset($parts['e'])) {
      $local_path .= check_plain($parts['a']) . '/' . check_plain($parts['e']) . '.jpg';
    }

    // Check if a local thumbnail is available and use it. Otherwise, create a
    // local copy of the original thumbnail.
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());

      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }

    return $local_path;
  }

}
