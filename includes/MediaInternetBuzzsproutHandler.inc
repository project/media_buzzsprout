<?php

/**
 * @file
 * Implements a media handler class for Buzzsprout audio.
 */

/**
 * Extends the MediaInternetBaseHandler class to handle Buzzsprout audio.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetBuzzsproutHandler extends MediaInternetBaseHandler {

  /**
   * Parses user-submitted embed code and returns a normalized file URI.
   *
   * @param string $embedCode
   *   A string of user-submitted embed code.
   *
   * @return mixed
   *   String containing the normalized URI, or FALSE if embed code is invalid.
   */
  public function parse($embedCode) {
    $patterns = array(
      '@buzzsprout\.com/([0-9]+)/(?>episodes/)?([0-9]+)@i',
    );

    foreach ($patterns as $pattern) {
      $uri = FALSE;

      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a file is added, but not every time the embedCode is parsed.
      preg_match($pattern, $embedCode, $matches);

      if (isset($matches[1]) && isset($matches[2])) {
        $uri = 'buzzsprout://a/' . $matches[1] . '/e/' . $matches[2];
      }

      if ($uri && self::validId($uri)) {
        return file_stream_wrapper_uri_normalize($uri);
      }
    }
  }

  /**
   * Determines if this handler should claim the item.
   *
   * @param string $embedCode
   *   A string of user-submitted embed code.
   *
   * @return bool
   *   Pass TRUE to claim the item.
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  /**
   * Returns a file object which can be used for validation.
   *
   * @return StdClass
   *   The file object.
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    return $file;
  }

  /**
   * Check if a Buzzsprout episode ID is valid.
   *
   * @return bool
   *   TRUE if the episode ID is valid, or throws a
   *   MediaInternetValidationException otherwise.
   */
  public static function validId($uri) {
    $uri = file_stream_wrapper_uri_normalize($uri);
    $external_url = file_create_url($uri);
    $url = url($external_url, array('query' => array('container_id' => 'player', 'player' => 'small')));
    $response = drupal_http_request($url, array('method' => 'HEAD'));

    if ($response->code != 304 && $response->code != 200) {
      throw new MediaInternetValidationException('The Buzzsprout episode could not be found.');
    }

    return TRUE;
  }

}
